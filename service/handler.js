'use strict';
const controller = require('./controller');

module.exports.hello = async event => {
  const name = event.queryStringParameters && event.queryStringParameters.name;
  const greet = controller.sayHello(name);

  return {
      statusCode: 200,
      body: greet
  };
};