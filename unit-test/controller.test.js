'use strict';

const greeter = require('../service/controller');

describe('sayHello', () => {
    test('Undefined name greet', () => {
        const greet = greeter.sayHello();
        expect(greet).toBe('Hello Mundo Cruel!');
    });

    test('Empty name greet', () => {
        const greet = greeter.sayHello('');
        expect(greet).toBe('Hello Mundo Cruel!');
    });

    test('Null name greet', () => {
        const greet = greeter.sayHello(null);
        expect(greet).toBe('Hello Mundo Cruel!');
    });

    test('With a name', () => {
        const greet = greeter.sayHello('Luis');
        expect(greet).toBe('Hello2 Friend Luis');
    });
});