'use strict';

const init = require('./utils/init');
const steps = require('./utils/steps');

const axios = require('axios');

describe('When we invoke the hello API', () => {  
    beforeAll(() => {
        init();
      });

    test('With a name', async () => {
        const response = await axios.get(process.env.BASE_URL + 'hello?name=Luis');
        expect(response.status).toBe(200);
        expect(response.data).toBe('Hello2 Friend Luis');
    });

    test('Without name', async () => {
        const response = await axios.get(process.env.BASE_URL + 'hello');
        expect(response.status).toBe(200);
        expect(response.data).toBe('Hello Mundo Cruel!');
    });
});