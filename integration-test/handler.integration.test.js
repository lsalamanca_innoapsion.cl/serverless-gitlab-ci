'use strict';

const init = require('./utils/init');
const steps = require('./utils/steps');

describe('When we invoke the hello function', () => {  
    test('With a name', async () => {
        var result =  await steps.invokeGetHello('Luis');
        expect(result.statusCode).toBe(200);
        expect(result.body).toBe('Hello2 Friend Luis');
    });
});